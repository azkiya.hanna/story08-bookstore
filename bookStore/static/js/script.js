function ajaxCall(value) {
    $('tbody').empty()
    let keyword = value
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + keyword,
        success: function(response) {
            for (let i = 0; i < response.items.length; i++) {
                let tempBody = document.createElement('tr')
                let tableNumber = document.createElement('td')
                tableNumber.innerText = i + 1
                tempBody.append(tableNumber)

                let bookTitle = document.createElement('td')
                bookTitle.innerText = response.items[i].volumeInfo.title

                let bookAuthor = document.createElement('td')
                if (response.items[i].volumeInfo.authors !== undefined) {
                    for (let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                        bookAuthor.innerText = response.items[i].volumeInfo.authors[j]
                    }
                } else {
                    bookAuthor.innerText = 'Author(s) Unknown'
                }
                let bookCover = document.createElement('td')
                if (response.items[i].volumeInfo.imageLinks !== undefined) {
                    let image = document.createElement('img')
                    image.src = response.items[i].volumeInfo.imageLinks.thumbnail
                    bookCover.append(image)
                } else {
                    let image = document.createElement('img')
                    image.alt = "No Cover Available"
                    bookCover.append(image)
                }
                tempBody.append(bookTitle, bookAuthor,bookCover)
                $('tbody').append(tempBody)            
            }
        },
        error: function() {
            console.log('Failed')
        }
    })
}

let button = $('#search-button')
let container = $('.content-container')

$(document).ready(ajaxCall('Alice in the wonderland'))

button.on('click', function() {
    $('.table-content').show()
    let keyword = $('#search-field').val()
    ajaxCall(keyword)
}) 
