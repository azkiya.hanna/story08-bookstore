from django.test import TestCase,Client
from django.urls import reverse,resolve
from .views import StoreFunction
class bookStoreTest(TestCase):
    
    def test_apakah_ada_url(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code,200)
    def test_apakah_file_html_digunakan(self):
        c = Client()
        response = c.get('')
        self.assertTemplateUsed(response, 'bookStore.html')
    def test_apakah_fungsi_StoreFunction_digunakan(self):
        response = resolve(reverse('bookStore'))
        self.assertEqual(response.func,StoreFunction)
    def test_apakah_ada_table_untuk_list_buku(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("<table",content)
    def test_apakah_ada_tombol_search(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("search-button",content)
    def test_apakah_ada_search_bar(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("search-field",content)



        
    
# Create your tests here.
