function ajaxCall(value) {
    $('tbody').empty()
    let keyword = value
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + keyword,
        success: function(response) {
            for (let i = 0; i < response.items.length; i++) {
                let tempBody = document.createElement('tr')
                let tempNo = document.createElement('td')
                tempNo.innerText = i + 1
                tempBody.append(tempNo)

                let tempTitle = document.createElement('td')
                tempTitle.innerText = response.items[i].volumeInfo.title

                let tempAuthor = document.createElement('td')
                if (response.items[i].volumeInfo.authors !== undefined) {
                    for (let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                        tempAuthor.innerText = response.items[i].volumeInfo.authors[j]
                    }
                } else {
                    tempAuthor.innerText = 'Author(s) Unknown'
                }
                let tempImg = document.createElement('td')
                if (response.items[i].volumeInfo.imageLinks !== undefined) {
                    let image = document.createElement('img')
                    image.src = response.items[i].volumeInfo.imageLinks.thumbnail
                    tempImg.append(image)
                } else {
                    let image = document.createElement('img')
                    image.alt = "No Cover Available"
                    tempImg.append(image)
                }
                tempBody.append(tempTitle, tempAuthor,tempImg)
                $('tbody').append(tempBody)            
            }
        },
        error: function() {
            console.log('Failed')
        }
    })
}

let button = $('#search-button')
let container = $('.content-container')

$(document).ready(ajaxCall('pokemon'))

button.on('click', function() {
    $('.table-content').show()
    let keyword = $('#search-field').val()
    ajaxCall(keyword)
}) 
